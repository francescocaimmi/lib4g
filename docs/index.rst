.. lib4g documentation master file, created by
   sphinx-quickstart on Tue Jul 30 12:56:43 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to lib4g's documentation!
=================================
Lib4g is a set of routines the author uses frequently in his daily work.
They cover functions from analysis of composite materials, fracture mechanics 
and data analysis.
This document describes the API of the library and provides a small tutorial for
the `composites` package.

Contents:

.. toctree::
   :maxdepth: 4
   
   modules.rst
   Composites package tutorial <composites_tutorial.rst>
   References <zzzreferences.rst>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

