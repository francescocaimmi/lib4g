lib4g package
=============

.. automodule:: lib4g
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    lib4g.LEFM
    lib4g.bin
    lib4g.composites
    lib4g.misc
    lib4g.statistics

Submodules
----------

lib4g.dataconv module
---------------------

.. automodule:: lib4g.dataconv
    :members:
    :undoc-members:
    :show-inheritance:

lib4g.dtreduction module
------------------------

.. automodule:: lib4g.dtreduction
    :members:
    :undoc-members:
    :show-inheritance:


