lib4g.composites package
========================

.. automodule:: lib4g.composites
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

lib4g.composites.elastic module
-------------------------------

.. automodule:: lib4g.composites.elastic
    :members:
    :undoc-members:
    :show-inheritance:

lib4g.composites.failure module
-------------------------------

.. automodule:: lib4g.composites.failure
    :members:
    :undoc-members:
    :show-inheritance:

lib4g.composites.lamination_theory module
-----------------------------------------

.. automodule:: lib4g.composites.lamination_theory
    :members:
    :undoc-members:
    :show-inheritance:


