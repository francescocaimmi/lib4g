lib4g.bin package
=================

.. automodule:: lib4g.bin
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

lib4g.bin.avg1D module
----------------------

.. automodule:: lib4g.bin.avg1D
    :members:
    :undoc-members:
    :show-inheritance:


