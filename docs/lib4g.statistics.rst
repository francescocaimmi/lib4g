lib4g.statistics package
========================

.. automodule:: lib4g.statistics
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

lib4g.statistics.analisi_voti module
------------------------------------

.. automodule:: lib4g.statistics.analisi_voti
    :members:
    :undoc-members:
    :show-inheritance:

lib4g.statistics.avg_curves module
----------------------------------

.. automodule:: lib4g.statistics.avg_curves
    :members:
    :undoc-members:
    :show-inheritance:

lib4g.statistics.errors module
------------------------------

.. automodule:: lib4g.statistics.errors
    :members:
    :undoc-members:
    :show-inheritance:


