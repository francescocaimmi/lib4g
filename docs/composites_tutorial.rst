.. _composites_tutorial_label:

Composites package tutorial
==============================

This is a short tutorial to illustrate the basic abilities of the lib4g 
composite subpackage.

Basic usage
------------

Working with linear elastic composites requires first the definition of material
properties. 
To define the plane stress compliance matrix  of carbon/epoxy composite starting
from the engineering constants it is possible to call:

.. ipython::

    In [1]: import numpy as np
    
    In [2]: from lib4g.composites.elastic import elastic_compliance_2D_pstress
    
    In [3]: E1, E2, v12, G12 = 181e3, 10.3e3, 0.0159, 7.17e3#MPa    
    
    In [5]: C = elastic_compliance_2D_pstress(E1, E2, v12, G12)
    
    In [6]: Q = np.linalg.inv(C)
    
    In [7]: Q

Note that the convention placing `v12` in line 1 row 2 of the compliance matrix 
is followed.

Once the plane stress stiffness matrix is defined, it is possible to build a 
laminate and calculate the ABD matrix. 
In order to do that, a sequence of layer thickness a sequence of layer 
orientation and a sequence of material stiffness matrices are needed.

Assuming to have a laminate made with a single material with the stacking 
sequence [0/30/-45] with all the layers 5 mm thick, this can be accomplished 
using the `clt_ABD` matrix function:

.. ipython::
    
    In [12]: Q = np.linalg.inv(C)
    
    In [13]: hs = [5.0, 5.0, 5.0]
    
    In [14]: thetas = [0.0, 30.0, -45.0]

    In [17]: from lib4g.composites.lamination_theory import clt_ABD
    
    In [18]: A,B,D = clt_ABD(hs,Q,thetas,symmetric=False)
    
    In [19]: A
    Out[19]: 
        array([[ 1739182.30853693,   388312.76596707,    56638.99731608],
               [  388312.76596707,   453219.31054669,  -114067.78845071],
               [   56638.99731608,  -114067.78845071,   452501.63050232]])
        
    In [20]: B
    Out[20]: 
        array([[-3128847.75046655,   985576.08714949, -1071635.83165853],
                [  985576.08714949,  1157695.57616758, -1071635.83165853],
                [-1071635.83165853, -1071635.83165853,   985576.08714949]])
            
    In [21]: D
    Out[21]: 
        array([[ 33430981.69102532,   6459550.95592459, -5240181.24721751],
                [  6459550.95592459,   9319175.47870832,  -5595820.38423164],
                [ -5240181.24721751,  -5595820.38423164,   7663092.16596059]])

For laminates made of different materials, the function `clt_ABD` accepts as 
theory second argument a list or an array of stiffness matrices, rather than
a single matrix.

Assume now that a equibiaxial membrane load is acting on the laminate so that 
the membrane stress vector is `[1.0,1.0,0.0]` N/mm, with null bending stresses. 
The membrane and bending strains are easily obtained by inversion of the ABD 
matrix.

.. ipython::
    
    In [22]: ABD = np.empty((6,6))#assemble a single ABD matrix
    
    In [23]: ABD[:3,:3]=A
    
    In [23]: ABD[:3,3:]=B
    
    In [23]: ABD[3:,:3]=B
    
    In [23]: ABD[3:,3:]=D
        
    In [23]: gen_stress = np.array([1.0,1.0,0.0,0.0,0.0,0.0])
            
    In [26]: gen_strain = np.dot(np.linalg.inv(ABD), gen_stress)
            
    In [27]: gen_strain
    Out[27]: 
            array([  3.12413121e-07,   3.49188945e-06,  -7.59860180e-07,
                    2.97033750e-08,  -3.28508246e-07,   4.10161437e-07])


To calculate the stresses in the layers the utility function 
`clt_layers_stress` is provided.
By defaults this function outputs the stresses at the top and bottom of each 
layer; anyway an arbitrary sequence of through-the-thickness points can be 
provided (the coordinates of the top and bottom faces of each layer can be 
obtained using the function `layers-Z` in the lamination_theory subpackage).

.. ipython::
    
    In [28]: from lib4g.composites.lamination_theory import clt_layers_stress

        
    In [30]: ss = clt_layers_stress(gen_strain, hs, Q, thetas, symmetric=False)
        
    In [31]: ss
    Out[31]: 
            array([[ 0.03351324,  0.06187658, -0.02750463],
                   [ 0.05576659,  0.04531224, -0.01280034],
                   [ 0.06929561,  0.07391299,  0.03380758],
                   [ 0.14336586,  0.08101965,  0.08426124],
                   [ 0.12352777,  0.15627957, -0.1186796 ],
                   [-0.02546907, -0.01840104,  0.04091575]])
            
    In [32]: ss.shape
    Out[32]: (6, 3)
    
As expected, for a three layer laminate the function, without specification of 
the optional keyword argument `zs` returns 6 stress vectors, two for each layer 
(at the top and bottom).

If this stress state is to be verified, the criteria of the subpackage 
`composites.failure` can be used. For example, checking the stress at 
the bottom of the laminate against the maximum
stress criterion is as easy as

.. ipython::
    
    In [33]: from lib4g.composites.failure import max_stress
   
    In [39]: R = [1810.0,980.0,35.0,86.0,48.0]#failure stresses
    
    In [40]: max_stress(ss[0], R)
    Out[40]: array([  1.85156030e-05,   1.76790240e-03,   5.73013099e-04])
    
where the outputs are the failure indices for the three stress components 
(sign is taken into account internally, see `max_stress` documentation).

Advanced features
------------------

The `lamination_theory` subpackage provides (or will provide in the future) 
utility functions combining the basic functions described before.

For example, the class `beam` can be use to represent a beam with constant 
rectangular cross section under bending loads, following the theory developed in
the 4th Chapter of the Book by Reddy (:cite:`reddy2003mechanics`).

This class is initialised by assigning a lamination sequence and provides 
methods to calculate the bending and the interlaminar stresses.

This tutorial is to be completed.
