Lib4g is a heterogeneous set of python utility routines that the author 
(Francesco Caimmi, francesco.caimmi@polimi.it) uses in his daily work.
It contains routines for data analysis and interpolation, 
for **fracture mechanics**, for the analysis of **composite materials and 
structures** using CLT.
These are used to build more complicated routines for data 
analysis.
Hopefully this will grow over time.

At the moment this stuff is mainly useful for students of my composite course 
as a starting point for building lamination sequence optimization routines.

Works for python 2.7. Might work on python 3 but has not been tested!
Should work on Windows but my primary development environment is Linux, so I am 
not sure about it.

None of the routines available in the package are granted to work (but this 
should be mentioned in the routine documentation) nor their results are granted 
to be sensible on a physical/mathematical/whatever basis. 
Use at your own risk.

Downloading
==============
To obtain the software via git::
    
    git clone https://francesco_caimmi@bitbucket.org/francesco_caimmi/lib4g.git

To get a tarball visit 
the `Download page <https://bitbucket.org/francesco_caimmi/lib4g/downloads/>`_


Installation
==============

With administrative privileges if required, run::
    
    python setup.py install

See the `Python documentation <https://docs.python.org/2/install/>`_
and the setup script help (python setup.py install -h) for details on how to
customize the install process and the install location.


Documentation
==============
Under the `docs` directory there are files to build the modules documentation 
using `Sphinx <http://www.sphinx-doc.org/en/stable/>`_. 
The Sphinx BibTeX extension is required to build the documentation
(`here <https://github.com/mcmtroffaes/sphinxcontrib-bibtex/>`_).

Enter `docs` directory and run::
    
    make html

Place it wherever you wish. 
One day maybe I will add documentation building and installation to the setup
process, depending on my laziness.

In the meanwhile, you should be able to read it online at
`Read The Docs <http://lib4g.readthedocs.io/en/latest/index.html>`_

Dependencies
=============
The software relies on NumPy and SciPy; most of the dependencies are handles by
the setup script via `pip` (check them if you want to install them via you 
package manager). The only exception is SymPy which is used in the composite 
module to get symbolic stiffness matrices. This is not installed by default, 
since is a very large package.
If you want to use symbolic matrices install it manually.

Licensing
===========

This stuff if licensed using the original BSD license. 
A copy of the license is available in the file LICENSE.txt.
