# -*- coding: utf-8 -*-
"""
Provides a set of routines to operate on functions, returning callable objects
"""


def subtract_functions(f, g):
    """
    Returns a function that is the difference of two functions.

    Parameters
    ------------

    f: callable
        A function of one argument.

    g: callable
        A function of one argument.

    Return
    --------
    h: callable
        A callable object giving the difference of `f` and `g`.
    """
    return lambda x: f(x) - g(x)