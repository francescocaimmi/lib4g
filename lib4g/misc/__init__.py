"""
Miscellaneous utilities not suited to fit into other modules.
"""
from dist_arg import dist_arg

__all__=['dist_index']