# -*- coding: utf-8 -*-
"""
Created on Wed Dec  3 09:35:15 2014

@author: fcaimmi

Provides facilities for the data reduction of tests on materials

Routine Listing
===============
"""

import numpy as np
from scipy import stats
from scipy.interpolate import UnivariateSpline

def log_resample(n, data, b = 10.0, k = 1, s = 0):
    """
    Resamples a curve at equally spaced points on a log scale.
    Usefull for working with viscoelastic stuff.

    At the moment works only with x-y pairs. To perform the resampling
    uses interpolation on a linear scale. The interpolation limits coincide
    with the limits of the original data

    Parameters
    -----------
    n: int
        number of samples
    data: ndarray
        the original data in a nsamples_x2 shape

    b: float, optional
        Logarithm base. Default to 10

    k:  int, optional
        Degree of the smoothing spline. Must be <= 5. Defualts to 1.
        It is passed directly to `SciPy UnivariateSpline`

    s:  float or None, optional
        Positive smoothing factor used to choose the number of knots. Number
        of knots will be increased until the smoothing condition is satisfied:
        sum((w[i]*(y[i]-s(x[i])))**2,axis=0) <= s
        If None (default), s=len(w) which should be a good value if 1/w[i]
        is an estimate of the standard deviation of y[i].
        If 0, spline will interpolate through all data points, which is the
        default beahviour (no smoothing)
        It is passed directly to `SciPy UnivariateSpline`

    Returns
    --------
    new_data: ndarray
        a nx2 array with the data resampled

    """
    x0 = data[:,0]
    y0 = data[:,1]

    interpolating_function = UnivariateSpline(x0, y0, k = k, s = s)

    #define the log scale limits
    if x0[0] == 0:
        Istart = np.log10(x0[1])/np.log10(b)
    else:
        Istart = np.log10(x0[0])/np.log10(b)
    Iend = np.log10(x0[-1])/np.log10(b)

    x = np.logspace(Istart, Iend, base = b, num = n)
    y = interpolating_function(x)

    return np.column_stack((x,y))

def modulus(strain, stress, mode = 'secant', smin=0.0005, smax=0.0025,
            fulloutput = False):
    """
    Calculates the elastic modulus from small strain-stress data using
    interpolation

    Parameters
    ------------

    strain: np.array
        the strains. Dimension N

    stress: np.array
        the stresses. Dimension N

    mode: string
        one in "secant,interp". Deafults to secant. If "secant" the
        modulus is calculated according to ISO 527-1 using the line between
        two point. If "interp" is calculated through linear regression.

    smin: float
        the minimum strain for the evaluation.
        Defaults to 0.0005 according to ISO 527-1.

    smax: float
        the minimum strain for the evaluation.
        Defaults to 0.0025 according to ISO 527-1.

    fulloutput: bool
        wether or not to return the full output


    Returns
    -------
    E: float
        the value of the elastic modulus. Returns None if an error occurred

    intercept:float, optional
        the value of the intercept at zero strain (only if mode is interp,
        None otherwise)
    r: float, optional
        r value (only if mode is interp, None otherwise)
    p: float, optional
        p value (only if mode is interp, None otherwise)
    std_err: float, optional
        standard_error value (only if mode is interp, None otherwise)


    """
    #define the range limits
    a = strain.searchsorted(smin)#sorts from left
    check = np.argmin([strain[a-1],strain[a]])
    #find the actual closest point
    #if check is zero the value closest to smin is the one corresponding
    #to a-1
    if check == 0:
        a = a - 1

    b = strain.searchsorted(smax)
    check = np.argmin([strain[b-1],strain[b]])
    if check == 0:
        b = b - 1
    #calculate the modulus
    if mode == 'secant':
        E = (stress[b] - stress[a])/(strain[b]-strain[a])
        if fulloutput:
            return E, None, None, None, None
        else:
            return E
    elif mode == 'interp':
        E, intercept, r, p, std_err = stats.linregress(strain[a:b+1],
                                                           stress[a:b+1])
        if fulloutput:
            return E, intercept, r, p, std_err
        else:
            return E
    else:
        print 'mode must be one in [secant,interp]'
        return None




