#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
A script tha averages 1D curves by reading data from files. Is basically
a wrapper around avg_curves from the `statistics` module.

"""

import argparse
#add the upper directory to access the package
import os.path
import sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '..'))
import numpy as np

from lib4g.statistics.avg_curves import avg_curves

def main(files, k = 1, s = 0, p = None, o = 'curve.txt', f = False, d = '\t'):
    """
    Main module.

    Parameters
    -----------

    files: list of strings
        list of filenames to open in order to read the data
        Each file can contain multiple curves.Columns are interpreted as
        x-values, y-values, x-values,y-values and so on.
        An error is raised if an odd number of columns is present

    k:  int, optional
        Degree of the smoothing spline. Must be <= 5. Defualts to 1.
        It is passed directly to `SciPy UnivariateSpline`

    s:  float or None, optional
        Positive smoothing factor used to choose the number of knots. Number
        of knots will be increased until the smoothing condition is satisfied:
        sum((w[i]*(y[i]-s(x[i])))**2,axis=0) <= s
        If None (default), s=len(w) which should be a good value if 1/w[i]
        is an estimate of the standard deviation of y[i].
        If 0, spline will interpolate through all data points, which is the
        default beahviour (no smoothing)
        It is passed directly to `SciPy UnivariateSpline`

    p:  integer
        the number of sampling point required in the interval [`xmin`,`xmax`]
        where `xmin` and `xmax ` is the smallest intervall for which all
        of the curves are defined. If you whish to replace the values by the
        minimum and maximum values in the data, please use the -f switch
        If None is specified, the sampling will be performed by using the
        abscissae values for the first supplied curve. Defaults to None

    o:  string
        outpufile name. Defaults to ``curve.txt``

    f:  boolean
        If `False` the abscissae range is set to [`xmin`,`xmax`]
        where `xmin` and `xmax ` is the smallest intervall for which all
        of the curves are defined. If `True`, `xmin` and `xmax ` are chosen
        ar the minimum and maximum value among all of the abscissae. Defaults
        to `False`

    d:  string
        column delimiter in files. Defaults to tab(\\t).

    Returns
    --------
    avg: numpy array
        an array containing the averaged values of the ordinates of the curve

    std: numpy array
        Estimate of the standard deviation estimated for the averaged points
        calculated as an average of the interpolated values obtained at the
        abscissae specified by `x`
        Shape: x.shape()

    x:  numpy array
        abscissae at which the aveaging is performed

    output: textfile
        a text file containing three columns: x, avg and std

    Returns None if a file contains an even number of rows
    """
    #build the data vector
    curves = []

    for filename in files:
        #to avoid import errors replace missing values with NaN
        tmp = np.genfromtxt(filename, filling_values = np.nan,
                            missing_values = np.nan, delimiter = d)
        ncols = tmp.shape[1]
        #check if we have an even number of rows
        if ncols % 2 != 0:
            print "The file ",filename," has an uneven number of columns"
            sys.exit(1)

        #separate the curves
        i = 0
        while i < ncols:
            xvalues = tmp[:,i]
            yvalues = tmp[:,i+1]
            #remove NaN entries
            xvalues = xvalues[np.logical_not(np.isnan(xvalues))]
            yvalues = yvalues[np.logical_not(np.isnan(yvalues))]

            curve = np.column_stack((xvalues,yvalues))
            curves.append(curve)

            i+=2

    #build the x vector
    if p == None:
        x = curves[0][:,0]

    elif f == True:

        xdata = np.hstack([c[:,0] for c in curves])
        xmax = xdata.max()
        xmin = xdata.min()
        x=np.arange(xmin,xmax,(xmax-xmin)/p)

    else:
       #build the array of the minima and maxima
       min_values = np.hstack([c[:,0].min() for c in curves])
       max_values = np.hstack([c[:,0].max() for c in curves])
       #find the "largest" minimum and the "smallest" maximum
       xmin = min_values.max()
       xmax = max_values.min()
       x=np.arange(xmin,xmax,(xmax-xmin)/p)

    #build options dictionary
    avg, std = avg_curves(*curves, x = x, k = k, s = s)

    #save
    np.savetxt(o,np.column_stack((x,avg,std)),delimiter = d,
               header='x\ty\tstd')

    return x,avg,std

if __name__ == "__main__":

    avgparser = argparse.ArgumentParser(description=
    'A script tha averages 1D curves by reading data from files.')

    avgparser.add_argument('files', help =
                            "List of tab separated data files.\
                            Data shall be readable by numpy.genfromtxt.\
                            Each file can contain multiple curves.\
                            Columns are interpreted as x-values, y-values,\
                            x-values,y-values and so on. An error is raised\
                            if an odd number of columns is present",
                           nargs='+')

    avgparser.add_argument('-s','--smoothing',
                           help = "Smoothing factor. Defaults to 0 (no smoothing).\
                           See SciPy UnivariateSpline documentation\
                           for more information",
                           default = 0, type = int)

    avgparser.add_argument('-k','--order',
                           help = "Interpolating spline order.\
                           See SciPy UnivariateSpline documentation\
                           for more information",
                           default = 1, type = int)

    avgparser.add_argument('-p','--points',
                           help = "Point number for the interpolated curve.\
                           If None, the x-values for the first supplied curve\
                           will be used",
                           default = None, type = int)

    avgparser.add_argument('-f','--full',
                           help = "Use the full data range rather than\
                           the minimum range over which all the curves are\
                           defined. (Default: %(default)s)",
                          action='store_true', default = False
                          )

    avgparser.add_argument('-o','--output', help = "Output file name.",
                           default = "curve.txt")
    avgparser.add_argument('-d','--delimiter',
                           help = "File Column separator.(Defaults: tab)",
                           default = "\t")
    #TODO add support for reading times
    options = vars(avgparser.parse_args(sys.argv[1:]))

    main(options['files'],
         k = options['order'],
         s = options['smoothing'],
         p = options['points'],
         o = options['output'],
         f = options['full'],
         d = options['delimiter']
        )
