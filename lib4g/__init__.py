# -*- coding: utf-8 -*-
"""
Lib4g is a set of utility routines for data analysis.

Author Information
Francesco Caimmi, Ph.D.
Laboratorio di Ingegneria dei Polimeri
Politecnico di Milano - Dipartimento di Chimica, Materiali e Ingegneria
Chimica
"Giulio Natta"

P.zza Leonardo da Vinci, 32
I-20133 Milano
Tel. +39.02.2399.4711
Fax +39.02.7063.8173

francesco.caimmi@polimi.it
Skype: fmglcaimmi
GPG Public Key : http://goo.gl/64dDo

"""
__all__=["statistics","bin","composites"]
