# -*- coding: utf-8 -*-
"""
Provides failure criteria for composites. All of them work with stresses
referred to the **principal material reference system**.
"""
import numpy as np

def max_stress(s, R):
    """
    The maximum stress criterion failure index, normalised so that a value
    greater than one indicates failure.

    Parameters
    -----------

    s: array-like
        the vector containing the stress state in the principal material
        reference system:
        :math:`{\\sigma_{11},\\sigma_{22},\\sigma_{12}}`. These stresses should
        have their proper sign.

    R: array-like
        the vector containing the resistance constants in the principal
        material reference system in the following order:
        :math:`{X^+,X^-,Y^+,Y^-,S}`

    Returns
    --------
    F: nd.array
        A vector containing the three failure index for failure along
        directions 1,2 and shear. The index is normalised so that a value
        greater than one indicates failure.

    """
    Xp, Xm, Yp, Ym, Sh = R #unpack the resistance constants for clarity
    F = np.zeros(3)

    #stresses along direction 1
    if s[0] >= 0.0:
        F[0] = s[0]/Xp
    else:
        F[0] = abs(s[0]/Xm)

    #stresses along direction 2
    if s[1] >= 0.0:
        F[1] = s[1]/Yp
    else:
        F[1] = abs(s[1]/Ym)

    #shear
    F[2] = abs(s[2]/Sh)

    return F

def tsai_hill(s, R):
    """
    The Tsai-Hill failure index.

    Parameters
    -------------

    s: array like
        the vector containing the stress state in the principal material
        reference system: :math:`{\\sigma_{11},\\sigma_{22},\\sigma_{12}}`
    R:  array-like
        the vector containing the resistance in the principal maerial
        reference systems {X,Y,S}

    Returns
    ---------
    F: float
        the value of the Tsai-Hill failure index
    """

    return (s[0]/R[0])**2+(s[1]/R[1])**2+(s[2]/R[2])**2-s[0]*s[1]/(R[0]**2)

def tsai_wu(s, R):
    """
    The Tsai-Wu failure index.

    Parameters
    -------------

    s: array like
        the vector containing the stress state in the principal material
        reference system: :math:`{\\sigma_{11},\\sigma_{22},\\sigma_{12}}`
    R:  array-like
        the vector containing the resistance in the principal maerial
        reference systems and the interaction parameter F12
        {Xplus, Xminus, Yplus, Yminus, S, F12}. If F12 is given as None, it is
        calculated automatically as -1/sqrt(Xplus*Xminus*Yplus*Yminus)

    Returns
    ---------
    F: float
        the value of the Tsai-Wu failure index
    """
    if R[5] == None:
        R[5] = -1/np.sqrt(R[0]*R[1]*R[2]*R[3])

    return s[0]*(1.0/R[0]-1.0/R[1])+\
           s[1]*(1.0/R[2]-1.0/R[3])+\
           s[0]**2/(R[0]*R[1])+\
           s[1]**2/(R[2]*R[3])+\
           s[0]*s[1]*R[5]+\
           (s[2]/R[4])**2
