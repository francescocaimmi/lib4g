# -*- coding: utf-8 -*-
#Created on Thu Mar  5 13:06:59 2015
#@author: fcaimmi
"""
A module that allows working with the elastic properties of laminates

Provides
==========
* Reuters matrix (reuter)
"""

import numpy as np


reuter = np.array([[1.0,0,0],[0,1.0,0],[0,0,2.0]], dtype = float)

def elastic_compliance_3D(*args):
    """
    Returns the elastic compliance matrix of a othotropic material
    given the corresponding engineering constants in the principial material
    reference system.

    The engineering constants follows the conventions:

    * each modulus is assocted to a column
    * poisson ratio :math:`\\nu_{ij}` goes at row i, column j

    The matrix is written using Voigt notation, i.e. it sends the vector
    {s_1,s_2,s_3,s_4,s_5,s_6} where 4 = 23, 5 = 13, 6 = 12 into the
    corresponding strains with the engineering strains expressed as gammas.

    Parameters
    ----------
    args: array like
        sequence of floats containing the engineering constants in the
        following order:
        E_1,E_2,E_3,nu_12,nu_13,nu_23,G_12,G_23,G_13

    Returns
    -------
    C: array
        the 6x6 compliance matrix
    """

    E1,E2,E3,v12,v13,v23,G12,G23,G13 = args
    one = 1.0
    C = np.array(
    [
    [ one/E1, -v12/E2, -v13/E3, 0, 0, 0],#line1
    [ -v12/E2, one/E2, -v23/E3, 0, 0, 0],#line2
    [ -v13/E3, -v23/E3, one/E3, 0, 0, 0],#line3
    [ 0, 0, 0, one/G23, 0, 0],#line4
    [ 0, 0, 0, 0, one/G13, 0],#line5
    [ 0, 0, 0, 0, 0, one/G12],#line5
    ]
    )
    return C

def elastic_compliance_2D_pstress(*args):
    """
    Returns the plane stress elastic compliance matrix of a othotropic material
    given the corresponding engineering constants in the principial material
    reference system.

    The engineering constants follows the conventions:

    * each modulus is assocted to a column
    * poisson ratio :math:`\\nu_{ij}` goes at row i, column j

    The matrix is written using Voigt notation, i.e. it sends the vector
    {s_1,s_2,,s_6}  6 = 12 into the
    corresponding strains with the engineering strains expressed as gammas.

    Parameters
    ----------

    args: array like
        floats containing the engineering constants in the following order
        E_1,E_2,nu_12,G_12

    Returns
    -------

    C: array
        the 6x6 compliance matrix
    """
    E1,E2,v12,G12 = args
    one = 1.0
    C = np.array(
    [
    [ one/E1, -v12/E2,  0],#line1
    [ -v12/E2, one/E2,  0],#line2
    [ 0, 0,  one/G12],#line3
    ]
    )
    return C

def mat_trans_3Dvec_rot(theta):
    """
    Returns the 3D transformation matrix  A that maps a tensor to another
    tensor in a reference system rotated along direction 3. The tensors is
    expressed in Voigt's notation as a six component vector and the
    trasformatio rule is then given by

    .. math::

        \\sigma^{'} = A(\\theta) . \\sigma

    See :cite:`reddy2003mechanics`, pag. 91 and subsequent.

    Parameters
    -----------

    theta:float
        rotation angle (in rad)

    Returns
    --------

    A: np.array
        the transformation matrix
    """
    m = np.cos(theta)
    n = np.sin(theta)
    A = np.array([
    [ m**2, n**2, 0, 0, 0, -2*m*n],#line1
    [ n**2, m**2, 0, 0, 0, 2*m*n],#line2
    [ 0, 0, 1.0, 0, 0, 0],#line3
    [ 0, 0, 0, m, n, 0],#line4
    [ 0, 0, 0, -n, m, 0],#line5
    [ m*n, -m*n, 0, 0, 0, m**2-n**2],#line5
    ]
    )
    return A

def mat_trans_2Dvec_rot(theta):
    """
    Returns the transformation matrix  A that maps a plane stress vector
    (3 components) to the tensor in another reference system rotated with
    respect to the axis perpendicular to the tensor plane.
    The tensors is
    expressed in Voigt's notation as a three component vector and the
    trasformation rule is then given by

    .. math::

        \\sigma^{'} = A(\\theta) . \\sigma

    See :cite:`reddy2003mechanics`, pag. 91 and subsequent.
    It can be used to map stresses in a generic reference system
    to stresses in the Principal Material Reference System. It can also be used
    for strains if they are expressed using nominal shear strains and not
    engineering shear strains (i.e. gamma), otherwise use
    mat_trans_2Dstrain_rot.

    Parameters
    -----------

    theta:float
        rotation angle (in rad)

    Returns
    --------

    A: np.array
        the transformation matrix
    """
    m = np.cos(theta)
    n = np.sin(theta)
    A = np.array([
    [m**2, n**2, 2*m*n],
    [n**2, m**2, -2*m*n],
    [-m*n,m*n,m**2-n**2]
    ],dtype='float')
    return A

def mat_trans_2Dstrain_rot(theta):
    """
    Returns the matrix that transforms the plane strain components in the
    principal material reference system upon rotation along a direction
    perpendicular to the plane. Uses Voigt's notation for the strains, that are
    thus reduced to a 3 component vector.
    The shear strains are the engineering shear strains, i.e. gamma.

    Parameters
    -----------

    theta: float
        rotation angle (in rad)

    Returns
    --------

    A: np.array
        the transformation matrix
    """
    m = np.cos(theta)
    n = np.sin(theta)
    A = np.array([
    [m**2, n**2, m*n],
    [n**2, m**2, -m*n],
    [-2*m*n,2*m*n,m**2-n**2]
    ], dtype='float')
    return A
