# -*- coding: utf-8 -*-
"""
A module to work with composite laminates.
Provides utilities to work composite beams with rectangular cross section under
bending loads and utilities to calculate the ABD matrix of laminates.

"""
from __future__ import print_function
#third party imports
import numpy as np
try:
    from sympy.matrices import zeros
except ImportError:
    print('Unable to import SymPy. Attempts at using symbolic matrices \
        will fail.')

#lib4g imports
from elastic import mat_trans_2Dvec_rot
from elastic import mat_trans_2Dstrain_rot

class beam(object):
    """
    A class representing a straight composite beam with rectangular cross
    section.
    The beam axis is oriented along the x axis, the laminae are stacked along
    the z-axis and the orientation is measured from x-axis to y-axis

    Parameters
    -----------

    b: float
        the beam width

    l: float
        the beam length

    hs: array-like
        array of the heights of each laminate

    qs: list or numpy array
        list of the plane stress stiffness matrix of each layer, referred to
        the principal material reference system. If only one matrix is given
        the laminate will be assumed to be made with a single material.

    thetas: array-like
        the array or the list of angles of orientation of the principal
        material reference system  w.r.t. the global reference system.
        The angles shall be supplied in degrees.

    symbolic: bool, optional
        Allows sympy objects in the other arguments, usefull to perform
        symbolic computations. Defaults to False.

    symmetric: bool, optional
        flag to specify that the lamination sequence is symmetric. If Ture,
        only the lamination sequence below the reference plane should be
        provided. Defaults to True

    Attributes
    ---------------------

    A: nd array
        the A matrix

    B: nd array
        the B matrix

    D: nd.array
        the D matrix

    L: int
        the number of layers

    Qs: list
        the list of the stiffness matrices of each layer in the PMRS

    Qs_prime: list
        the list of the stiffness matrices of each layer in the beam reference
        system

    Zs:nd array
        the list of top and bottom layer positions with repect tot he midplane

    """
    def __init__(self, b, l, hs, qs, thetas, symbolic = False,
                 symmetric = True):

        if symbolic == True:
            mydtype = 'object'
        else:
            mydtype = None

        self.l = float(l)
        self.b = float(b)
        #determine the number of layers
        L = len(hs)
        if symmetric:
            self.L = 2*L
            self.hs = np.concatenate((hs,hs[::-1]))
            self.thetas = np.concatenate((thetas,thetas[::-1]))
        else:
            self.L = L
            self.hs = hs
            self.thetas = thetas

        #check if we have a laminate made with a single material or not
        if type(qs) is not list:
            #qs is a single matrix
            self.Qs = [qs for i in range(self.L)]
        else:
            #qs is a list
            self.Qs = qs
            if symmetric:
                self.Qs = self.Qs + self.Qs[::-1]

        self.Qs_prime = []
        #rotate the Qs in the beam reference system
        for k in range(self.L):
             theta = self.thetas[k]
             Tinv = np.linalg.inv(mat_trans_2Dvec_rot(theta))
             Te = mat_trans_2Dstrain_rot(theta)
             Qk = np.dot(Tinv, np.dot(self.Qs[k],Te))
             self.Qs_prime.append(Qk)

        #prepare an array with the through the thickness quotas
        self.Zs = layers_Z(hs,zdtype=mydtype)
        #symmetric is false since we already created the complete stacking
        #sequence before calling
        self.A, self.B, self.D = clt_ABD(self.hs, self.Qs, self.thetas,
                                         symbolic= symbolic, symmetric=False)

    def bending_stresses(self, M, zs = None):
        """
        Provides the in-plane stresses in a beam assuming that the only
        load is provided by bending perpendicular to the beam axis or
        transverse forces through the thickness.

        Parameters
        -----------

        M: float
            the bending moment

        zs: array like
            the points where the bending stresses must be calculated. If None
            results are provided at the top and the bottom of each layer.
            Defaults to None

        Returns
        --------

        sigma: nd array
            an array of shape (len(Zs),3) providing the stresses at each point
            Z. If a point in Zs is at the boundary of a layer, the results
            for the two layers are provided consecutively

        """
        if zs == None:
            zs = self.Zs


        Dstar = np.linalg.inv(self.D)
        #define the layers to which each z belogs
        positions = []#list of tuples (quote,layer index)
        for z in zs:
            for k in range(self.L):
                if z >= self.Zs[k] and z <= self.Zs[k+1]:
                    #the position z is in this layer
                    positions.append((z,k))
#                    #if z is equal to the top layer position, we must provide
#                    #also the value for the stress at the bottom at layer
#                    #k+1
#                    if z == self.Zs[k+1]:
#                        positions.append((z,k+1))

        sigma = np.zeros((len(positions),3))
        for i,p in enumerate(positions):
            z,k = p
            Qprime = self.Qs_prime[k]
            sigma[i,:] = (M*z/self.b)*np.dot(Qprime,Dstar[0,:])



        return sigma

    def interlaminar_stresses(self,V,dV,zs=None,
                              xzbc_bottom=0, yzbc_bottom=0, zzbc_bottom=0,
                              xzbc_top=0, yzbc_top=0, zzbc_top=0):
        """
        Provides the interlaminar stresses in a beam assuming that the only
        load is provided by bending perpendicular to the beam axis or
        transverse forces through the thickness.

        Parameters
        -----------

        V: float
            the transverse load

        dV: float
            the transverse load derivative

        zs: array like, optional
            the points where the bending stresses must be calculated. If None
            results are provided at the top and the bottom of each layer.
            Defaults to None

        aabc_top,aabc_bottom: float, optional
            the boundary conditions for the interlaminar stress component
            aa at the top or bottom of the laminate

        Returns
        --------

        sigma: nd array
            an array of shape (len(Zs),3) providing the stresses at each point
            Z. The stress components returned are sigma_xz, sigma_yz, sigma_zz.

        """
        if zs==None:
            zs = self.Zs

        loads = np.array([-V/self.b,-V/self.b,-dV/self.b])

        Dstar = np.linalg.inv(self.D)
        #define the layers to which each z belogs
        positions = []#list of tuples (quote,layer index)
        for z in zs:
            for k in range(self.L):
                if z >= self.Zs[k] and z <= self.Zs[k+1]:
                    #the position z is in this layer
                    positions.append((z,k))
                    break

        #find the arbitrary constants in the stress expression
        Cs = np.zeros((self.L,3))
        for k in range(self.L):
            if k == 0:
                Cs[k,:] = np.array([xzbc_bottom,yzbc_bottom,zzbc_bottom])
            else:
                zk = self.Zs[k-1]
                zk1 = self.Zs[k]
                Qk = self.Qs_prime[k-1]
                QD = np.dot(Qk,Dstar[0,:])
                quotes = np.array([(zk1**2-zk**2)/2.0, (zk1**2-zk**2)/2.0,
                        (zk1**3-zk**3)/6.0])

                Cs[k,:] = loads*QD*quotes+Cs[k-1,:]


        sigma = np.zeros((len(positions),3))
        for i,p in enumerate(positions):
            z,k = p
            zk = self.Zs[k]
            Qprime = self.Qs_prime[k]
            QD = np.dot(Qprime,Dstar[0,:])
            quotes = np.array([(z**2-zk**2)/2.0, (z**2-zk**2)/2.0,
                        (z**3-zk**3)/6.0])
            sigma[i,:] = loads*QD*quotes+Cs[k,:]

        return sigma

def clt_ABD(hs, qs, thetas, symbolic = False, symmetric = True):
    """
    Provides the Classical Lamination theory ABD matrix for a laminate.
    Equations assume that the referene plane is the laminate midplane.

    Parameters
    -----------

    hs: array-like
        array of the heights of each lamina in the stack

    Qs: list or numpy array
        list of the plane stress stiffness matrix of each layer, referred to
        the principal material reference system. If only one matrix is given
        the laminate will be assumed to be made with a single material.

    thetas: array-like
        the array or the list of angles of orientation of the principal
        material reference system  w.r.t. the global reference system.
        Must be supplied in degrees.

    symbolic: bool, optional
        Allows sympy objects in the other arguments, usefull to perform
        symbolic computations. Defaults to False.

    symmetric: bool, optional
        flag to specify that the lamination sequence is symmetric. If True,
        only the lamination sequence below the reference plane should be
        provided. Defaults to True.

    Returns
    --------

    A,B,D: array or sympy matrix
        the A, B and D matrices. They are numpy arrays if symbolic is False,
        sympy matrices otherwise
    """
    #check that the input is sensible
    hs = np.array(hs)
    assert (hs >= 0 ).all(), "All the layer thicknesses must be positive"

    #handle symbolic variables in hs, qs, thetas
    if symbolic == True:
        mydtype = 'object'
        A = zeros(3)
        B = zeros(3)
        D = zeros(3)
    else:
        mydtype = None
        A = np.zeros((3,3), dtype = mydtype)
        B = np.zeros((3,3), dtype = mydtype)
        D = np.zeros((3,3), dtype = mydtype)


    #determine the number of layers
    L = len(hs)
    if symmetric:
        L = 2*L
        hs = np.concatenate((hs,hs[::-1]))
        thetas = np.concatenate((thetas,thetas[::-1]))
    #convert angles to rad if there are no symbols in theta
    try:
        thetas = np.deg2rad(thetas)
    except AttributeError:
        pass

    #prepare an array with the through the thickness quotas
    Z = layers_Z(hs, zdtype=mydtype)

    #check if we have a laminate made with a single material or not
    if type(qs) is not list:
        #qs is a single matrix
        Qs = [qs for i in range(L)]
    else:
        #qs is a list
        Qs = qs
        if symmetric:
            Qs = Qs + Qs[::-1]


    #cycle over the layers
    for k in range(L):
        #rotate the stiffness matrix
        theta = thetas[k]
        Tinv = np.linalg.inv(mat_trans_2Dvec_rot(theta))
        Te = mat_trans_2Dstrain_rot(theta)

        Qk = np.dot(Tinv, np.dot(Qs[k],Te))
        A += Qk*hs[k]
        D += (1.0/3.0) * Qk * (Z[k+1]**3-Z[k]**3)

        if not symmetric:
            B += (1.0/2.0) * Qk * (Z[k+1]**2-Z[k]**2)
    #done

    return A, B, D

def clt_layers_stress(deformation, hs, Qs, thetas, symmetric=True, zs=None):
    """
    Given a stacking sequence and a strain state, calculates the corresposnding
    stress through the thickness at some sampling points, using classical
    lamination theory.
    By default gives the results at top and bottom of each layer in the stack,
    but different points can be selected.
    If a point lays at the interface of two different layers, the results for
    both layers will be provided consecutively in the results vector, which
    therefore may have more elements than those in zs.


    Parameters
    -----------

    deformation: array-like, shape (6,)
        The array containing the three membrane deformations and the three
        bending deformation in this order as
        :math:`\\lbrace \\eta_{1}, \\eta_{2}, \\eta_{12},
                \\chi_{1}, \\chi_{2}, \\chi_{12} \\rbrace`

    hs: array-like
        Array of the heights of each lamina in the stack.

    Qs: list or numpy array
        list of the plane stress stiffness matrix of each layer(each one
        should be a (3,3) numpy array), referred to
        the principal material reference system.
        If only one matrix is given
        the laminate will be assumed to be made with a single material.

    thetas: array-like
        the array or the list of angles of orientation of the principal
        material reference system  w.r.t. the global reference system.
        Must be supplied in degrees.

    symmetric: bool, optional
        flag to specify that the lamination sequence is symmetric. If True,
        only the lamination sequence below the reference plane should be
        provided. Defaults to True.

    zs: array-like, or None. Optional.
        the list of the positions where the stresses will be evaluated. Can
        be a single value (in that case the output will be a single vector).
        If None is given, the stresses at the top and bottom positions for
        each layer will be returned. Defaults to None.

    Returns
    --------

    stress: nd.array
        The in plane stresses for the points zs.
        There can be more results than `len(zs)` as if some z is at an interface
        between layers, stresses for both the layer k and k+1 will be provided.
        For example if zs is None, than zs will contain L+1 layers but stress
        will contain 2*L-2 stress vectors, two for each zs at the interfaces
        except the top and bottom laminate position which appear only once
        (L number of layers).
    """

    #input handling
    L = len(hs)

    try:
        #Qs has a shape, so is an array
        Qshape = Qs.shape
        if Qshape == (3,3):
            #this is a single stiffness matrix, so we have to build an
            #array of matrices for each layer
            Qs = np.array([Qs for i in range(L)])
        #otherwise we have a numpy array with stiffness matrices at each entry,
        #so everything s already setup
    except AttributeError:
        #in this case Qs must be a list, so nothing to do
        pass

    if symmetric:
        L = 2*L
        hs = np.concatenate((hs,hs[::-1]))
        thetas = np.concatenate((thetas,thetas[::-1]))
        Qs = np.concatenate((Qs,Qs[::-1]))

    if zs is None:
        zs = layers_Z(hs, symmetric=False)#symmetry already handled
    elif not isinstance(zs, (list, tuple, np.ndarray)):
        #we have a single layer, wrap it into a single element array
        zs = np.array([zs])

    #create the list of layers top and bottom position
    layerZs = layers_Z(hs, symmetric=False)
    #TODO should we add a check to verify that the zs are inside the laminate??
    #check that the zs are actually inside the laminate
    #Ztop, Zbot = layerZs[-1], layerZs[0]

    #now we must determine the positions were the stress must be calculated
    #at their correspoding layer. If a point is at the interface, we must
    #return the results for both the inferior and the superior layer.
    #this will be a  list of tuples (coordinate, layer index)
    #TODO should this be factored out in a dedicated function?
    #we have similar code also in stress functions of beam class.
    positions = []
    for z in zs:
        #cycle over the layers from the bottom
        for k in range(L):
            if z >= layerZs[k] and z <= layerZs[k+1]:
                #z is in layer k, append z to position list
                positions.append((z,k))
                if z == layerZs[k+1] and k+1<L:
                    #add also for upper layer
                    positions.append((z,k+1))
                break

    #cycle over the positions to get the stress
    stress = np.zeros((len(positions), 3))
    #we need eta and chi as array so force conversion in case deformation is
    #list
    eta = np.array(deformation[:3])
    chi = np.array(deformation[3:])
    for i, val in enumerate(positions):
        z, k = val
        theta = np.deg2rad(thetas[k])
        epsilon = eta + z * chi
        #transformation matrices
        Te = mat_trans_2Dstrain_rot(theta)
        T = mat_trans_2Dvec_rot(theta)
        #rotate the stiffness matrix in the global reference system
        Qk = np.dot(np.linalg.inv(T),np.dot(Qs[k],Te))
        stress[i] = np.dot(Qk, epsilon)

    #if we have a single zs, we want a single array output
    if stress.shape[0] == 1:
        return stress[0]
    else:
        return stress


def layers_Z(hs, symmetric=False, zdtype=None):
    """
    Given a sequence of layer thicknesses, returns the through the thickness
    coordinates of the top and bottom faces of each layer, assuming the
    mid-plane as the reference plane(Z=0).

    Parameters
    -----------

    hs: array like
        The lamination sequence thickness.

    symmetric: bool, optional
        Wheter or not the laminations sequence is symmetric. Defaults to False.

    zdtype: np.dtype, optional
        The dtype of the returned array. Defaults to None, which means that the
        default numpy dtype will be used. Useful for symbolic computations.


    Returns
    ----------

    Z: nd array
        The array with the requested coordinates.

    """

    L = len(hs)

    if symmetric:
        L = 2*L
        hs = np.concatenate((hs,hs[::-1]))

    if zdtype is not None:
        Z = np.zeros(L+1, dtype=zdtype)
    else:
        Z = np.zeros(L+1)

    h = np.sum(hs)#total thickness
    Z[0] = -h/2.0#position of the first layer
    for i in range(L):
        Z[i+1] = hs[i] + Z[i]

    return Z


def moduli_carpet_plot(prop, t, angles, Q,
                        xaxis ='alpha', param='beta',
                        freq=0.1, outfile='carpet.pdf', step=0.05,
                        symmetric=False, unit=''):
    """
    Produces a carpet plot for the equivalent elastic properties of  laminates
    of the form
    :math:`[\\theta0_{\\alpha}, \\theta1_{\\beta}, \\theta2_{\\gamma}]`.
    The laminate may be symmetric, but only three angles are supported.

    Parameters
    ----------

    prop: string
        The elastic property to be plotted: EX, EY, GXY or nuXY for normal
        deformation, EXb, EYb, GXYb or nuXYb for bending moduli.

    t: float
        The total thickness of the laminate.

    angles: list of strings
        The array or the list of angles of orientation of the principal
        material reference system  w.r.t. the global reference system.
        The angles shall be supplied in degrees as a list of **strings**;
        to indicate the presence of layers at :math:`\\pm\theta` prepend the
        letters `pm` to the desired angle, e.g. `pm45`.

    Q: array-like
        The stiffness matrix of the lamina in the principal material reference
        system

    xaxis: string, optional
        The variable to be plot on the x-axis of the carpet plot. One in
        [alpha,beta,gamma], the volume fractions of the layers. Defaults to
        alpha

    param: string, optional
        The other contour variable. One in
        [alpha,beta,gamma], the volume fractions of the layers. Defaults to
        beta

    freq: float, optional
        The frequency at which countour lines are plotted. Defaults to 0.1.

    outfile: string, optional
        The plot file name.
        Defaults to 'carpet.pdf'

    step: float, optional
        The sampling length along `xaxis` . Defaults to 0.05.

    symmetric: bool, optional
        If `True`, the lamination sequence will be assumed to be symmetric. `t`
        is still the **total** thickness

    unit:string, optional
        The measuring unit to be placed on the y-axis. Defaults to an empty
        string. If you want brackets, add them.

    Returns
    --------
    values: list
        a list of arrays containing the x and the y values for each level.
        The last element of the list is the envelope for the third parameter,
        i.e. the one not specified in `xaxis` and `param` equal to zero.

    """
    #TODO should we add support for list of properties?
    import matplotlib.pyplot as plt
    #a dictionary mapping property names to label for the y-axis
    prop2label = {'EX':'$E_X$','EY':'$E_Y$',
                  'GXY':'$G_{XY}$', 'nuXY': '$\\nu_{XY}$',
                  'EXb':'$E_{X,b}$','EYb':'$E_{Y,b}$',
                  'GXYb':'$G_{XY,b}$', 'nuXYb': '$\\nu_{XY,b}$',
                    }
    #check the input
    assert len(angles) <= 3, 'Only stacking sequences up to three angles are supported'
    #process the input angles
    thetas = []
    plottitle = []
    pm = []#flag to indicate if a given layer is actualy a +/- layer
    for angle in angles:
        if 'pm' in angle:
            val = angle.split('pm')[-1]
            thetas += [float(val),-float(val)]
            pm.append(True)
            plottitle.append('$\\pm'+val+'$')
        else:
            thetas.append(float(angle))
            pm.append(False)
            plottitle.append('$'+angle+'$')
    thetas = np.array(thetas)
    nlayers = len(thetas)
    plottitle = '['+','.join(plottitle)+']'
    if symmetric:
        t=t/2.0
        plottitle += '$_S$'
    #initialize loop and global variables
    hsthreshold = 1e-9
    fig = plt.figure(figsize=(6,6))
    x = np.arange(0,1+step,step)
    levels = np.arange(0.0,1.0+freq,freq)
    Qs = [Q]*nlayers
    param_label_flag = False
    values = []
    envelopex=np.zeros_like(levels)
    envelopey=np.zeros_like(levels)

    for lindex, l in enumerate(levels):
        y = np.zeros_like(x)
        mask = len(x)
        #determine the thickness of the stacking sequnce for x,l, tacking into
        #account the +/- sequences
        for i in range(len(x)):
            hs = np.zeros(nlayers)
            if xaxis == 'alpha':
                #thickenss of layers at alpha
                if pm[0]==False:
                    hs[0] = x[i]*t
                    ibeta = 1
                else:
                    hs[0] = x[i]*t/2.0
                    hs[1] = hs[0]
                    ibeta = 2
                #determine the thickness of the other layers
                if param == 'beta':#case of the beta as a parameter
                    #fraction of layers at beta
                    if pm[1] == False:
                        hs[ibeta] = l*t
                        igamma = ibeta + 1
                    else:
                        hs[ibeta] = l*t/2.0
                        hs[ibeta+1] = hs[ibeta]
                        igamma = ibeta + 2
                    #fraction of layers at gamma
                    if pm[2]==False:
                        #works beacause hs is zeroed at every cycle
                        hs[igamma] = t - np.sum(hs)
                    else:
                        print(t,hs)
                        hs[igamma] = (t - np.sum(hs))/2.0
                        hs[igamma+1] = hs[igamma]
                else:#parameter == gamma
                    if pm[1] == False:
                        igamma = ibeta + 1
                        if pm[2] == False:
                            hs[igamma] = l*t
                        else:
                            hs[igamma] = l*t/2.0
                            hs[igamma+1] = hs[igamma]
                        hs[ibeta] = t -np.sum(hs)
                    else: #pm[1] == True
                        igamma = ibeta + 2
                        if pm[2] == False:
                            hs[igamma] = l*t
                        else:
                            hs[igamma] = l*t/2.0
                            hs[igamma+1] = hs[igamma]
                        hs[ibeta] = 0.5*(t -np.sum(hs))
                        hs[ibeta+1] = 0.5*(t -np.sum(hs))

            elif xaxis == 'beta':
                #thickness of layers at beta
                if pm[0] == False:
                    ibeta = 1
                else:
                    ibeta = 2

                if pm[1] == False:
                    hs[ibeta] = x[i]*t
                    igamma = ibeta + 1
                else:
                    hs[ibeta] = x[i]*t/2.0
                    hs[ibeta+1] = hs[ibeta]
                    igamma = ibeta + 2

                #thickness of other layers
                if param == 'alpha':
                    #thicknes of layer at alpha
                    if pm[0] == False:
                        hs[0] = l*t
                    else:
                        hs[0] = l*t/2.0
                        hs[1] = hs[0]
                    #thickness at gamma
                    if pm[2] == False:
                        hs[igamma] = t-np.sum(hs)
                    else:
                        hs[igamma] = 0.5*(t-np.sum(hs))
                        hs[igamma+1] = hs[igamma]
                else:#parameter == gamma
                    #thickness of layer at gamma
                    if pm[2] == False:
                        hs[igamma] = l*t
                    else:
                        hs[igamma] = 0.5*l*t
                        hs[igamma+1] = hs[igamma]
                    #thicknes of layers at alpha
                    if pm[0] == True:
                        hs[0] = t - np.sum(hs)
                    else:
                        hs[0] = 0.5*(t - np.sum(hs))
                        hs[1] = hs[0]
            else: #axis == gamma
                #thickness of layers at gamma
                if pm[0] == False:
                    ibeta = 1
                else:
                    ibeta = 2
                if pm[1] == False:
                    igamma = ibeta+1
                else:
                    igamma = ibeta+2
                if pm[2] == False:
                    hs[igamma] = x[i]*t
                else:
                    hs[igamma] = 0.5*x[i]*t
                    hs[igamma+1] = hs[igamma]
                if param == 'alpha':
                    #thicknees of layers at alpha
                    if pm[0] == False:
                        hs[0] = l*t
                    else:
                        hs[0] = 0.5*l*t
                        hs[1] = 0.5*l*t
                    #thickness of layers at beta
                    if pm[1] == False:
                        hs[ibeta] = t-np.sum(hs)
                    else:
                        hs[ibeta] = 0.5*(t-np.sum(hs))
                        hs[ibeta+1] = hs[ibeta]
                else:#param == beta
                    #thickness of layers at beta
                    if pm[1] == False:
                        hs[ibeta] = l*t
                    else:
                        hs[ibeta] = 0.5*l*t
                        hs[ibeta+1] = hs[ibeta]
                    #thicknes at alpha
                    if pm[0] == False:
                        hs[0] = t-np.sum(hs)
                    else:
                        hs[0] = 0.5*(t-np.sum(hs))
                        hs[1] = hs[0]

            #we neeed to chek if all the thicknesses are positive
            #if there is one which is not we reached the end point for level l
            #however we need some tolerance because very small negative numbers
            #may show up in hs
            lowindices = np.abs(hs)<hsthreshold
            hs[lowindices]=0.0

            if (hs<0).any():
                mask = i
                break

            #laminate is fully prepared no need for the symmetric flag
            A,B,D = clt_ABD(hs,Qs,thetas, symmetric=symmetric)

            if prop == 'EX':
                y[i] = (A[0,0]*A[1,1]-A[0,1]**2)/(t*A[1,1])
            elif prop == 'EY':
                y[i] = (A[0,0]*A[1,1]-A[0,1]**2)/(t*A[0,0])
            elif prop == 'GXY':
                y[i] = A[2,2]/t
            elif prop == 'nuXY':
                y[i] = A[0,1]/A[1,1]
            elif prop == 'EXb':
                y[i] = 12*(D[0,0]*D[1,1]-D[0,1]**2)/(t**3*D[1,1])
            elif prop == 'EYb':
                y[i] = 12*(D[0,0]*D[1,1]-D[0,1]**2)/(t**3*D[0,0])
            elif prop == 'GXYb':
                y[i] = 12*A[2,2]/t**3
            elif prop == 'nuXYb':
                y[i] = D[0,1]/A[1,1]
        #we need tp plot the line at the second parameter equal to zero, which
        #is equal to the locus of the end points of the plots for each level
        #which therefore we store separately
        envelopex[lindex] = x[:mask][-1]
        envelopey[lindex] = y[:mask][-1]
        #store the results
        values.append([x[:mask],y[:mask]])
        mycolor = 'C'+str(lindex%10)
        #we place the point for level = 1.0
        if mask == 1:
            plt.plot(x[0], y[0], 'o', color=mycolor)
        else:
            plt.plot(x[:mask], y[:mask], color=mycolor)

        #level labels
        #TODO: find a way to displace sligthly the labels, if it makes sense.
        if param_label_flag == False:
            label = 'Fraction at $\\' + param + '$: ' + str(l)
            param_label_flag = True
        else:
            label = l
        plt.text(x[mask/2], y[mask/2], label, color=mycolor, fontsize='large')
    #add the envelope plot
    plt.plot(envelopex,envelopey,color='k')
    if xaxis == 'alpha':
        if param == 'beta':
            env_label = 'Line for $\\gamma=0$'
        else:
            env_label = 'Line for $\\beta=0$'
    elif xaxis == 'beta':
        if param == 'alpha':
            env_label = 'Line for $\\gamma=0$'
        else:
            env_label = 'Line for $\\alpha=0$'
    else:#xaxis == 'gamma'
        if param == 'beta':
            env_label = 'Line for $\\alpha=0$'
        else:
            env_label = 'Line for $\\beta=0$'
    plt.text(envelopex[len(envelopex)/2],envelopey[len(envelopex)/2],
             env_label,color='k',fontsize='large'
            )
    #set sensible axis limits
    plt.xlim(0,1)
    #decorate the plot
    plt.ylabel(prop2label[prop] + ' '+unit,fontsize='xx-large')
    plt.xlabel('Fraction of layers at $\\'+xaxis+'$',fontsize='xx-large')
    plt.xticks (fontsize='large')
    plt.yticks(fontsize='large')
    plt.title(plottitle)
    plt.savefig(outfile, bbox_inches='tight')
    #store the envelope
    values.append([envelopex,envelopey])
    return values
    #TODO add support for custom color cycles