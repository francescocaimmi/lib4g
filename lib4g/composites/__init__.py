# -*- coding: utf-8 -*-
"""
A package that provides modules to work with composite materials.

A tutorial on how to use this module is available following
:ref:`this link<composites_tutorial_label>`.
"""

import elastic
import failure
import lamination_theory

