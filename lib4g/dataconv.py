# -*- coding: utf-8 -*-
"""
Converters for different data format

Author: Francesco Caimm
francesco.caimmi@polimi.it
"""
import numpy as np
from collections import OrderedDict

def _CEAST_reader(lines,filetype='DAS4000'):
    """
    Given a list of lines from CEAST files, returns a data array and a test
    parameters dictionary.

    Parameters
    -----------
    lines: list
        list of strings from the CEAST files

    filetype: string
        'DAS4000' or 'DAS8000' parameters are sensibile ornly for DAS 4000 atm
        to be implmented

    Returns
    --------
    param_dict: ordered dictionary
        a dictionary with the test parameters

    data: np.array
        array with the data
    """
    #separate the data and parameters lines
    parastart = 0
    paraend = 0
    data_start = None
    for index, line in enumerate(lines):
        l=line.strip()#remove trailing spaces or newline
        if l == "Internal":
            #we reached the trigger line
            parastart = index + 1

        if l == '' and parastart != 0:
            paraend = index - 1
            data_start = index + 1

        if data_start:
            break


    parameter_lines = lines[parastart:paraend]
    data_lines = lines[data_start:]
    #create a parameter dictionary
    par_keys = ['head', 'max_range[kN]','range[kN]', 'mass[kg]',
                'falling height [m]', 'boh1', 'impact speed[m/s]',
                'impact energy[J]','boh2','boh3','boh4','boh5']
    par_dict = OrderedDict()
    for i,line in enumerate(parameter_lines):
        l = line.strip()
        if i==0:#line 1 is the head type, a string
            par_dict[par_keys[i]] = l
        else:
            par_dict[par_keys[i]] = float(l)


    #remove unknonwn parameters
    for key in par_dict.keys():
        if 'boh' in key:
            par_dict.pop(key)


    #read the datalines
    data = []
    ds = []

    for line in data_lines:
        l = line.strip()
        if l != '':
            ds.append(float(l))
        else:
            data.append(ds)
            ds = []


    data_type = [('time','f8'), ('force','f8'),('velocity','f8'),
                 ('energy','f8')]
    time = data[3]
    force = data[0]
    energy = data[1]
    velocity= data[2]
    data = np.array(zip(time,force,velocity,energy), dtype=data_type)

    return par_dict, data


def CEAST_to_txt(filename,filetype='DAS4000'):
    """
    Converts CEAST txt file as output from our instron to a file with the same
    name and dat extension, with a sensible column ordering.
    The file is tab separated.

    Parameters
    -----------

    filename: string
        The file to convert.

    filetype: string
        'DAS4000' or 'DAS8000' parameters are sensibile ornly for DAS 4000 atm
        to be implmented.

    Returns
    --------
    None
    """
    #read
    with open(filename, mode='r') as f:
        lines = f.readlines()
        par_dict, data = _CEAST_reader(lines)

    #write
    newfilename = filename.split('.')[0]+'.dat'
    header = '\t'.join(['Parameter','Value','time [ms]','force [N]',
                        'velocity [m/s]', 'energy [J]'])+'\n'

    par_keys = par_dict.keys()

    with open(newfilename, mode = 'w') as f:
        f.write(header)
        for i in xrange(len(data)):
            if i < len(par_dict):
                key = par_keys[i]
                value = str(par_dict[key])
                output = [key, value] + map(str,data[i])
                line = '\t'.join(output) + '\n'
                f.write(line)
            else:
                output = ['', ''] + map(str,data[i])
                line = '\t'.join(output) + '\n'
                f.write(line)






