# -*- coding: utf-8 -*-
"""
The module provides utilities for the evaluation of some error estimates for
statistical analysis of sample datas

"""
import numpy as np

def linregress_errors(x,y, A, B):
    """
    Gives the standard error for the estimates of the two parameters of
    a linear model fit of the form A+Bx. See
    `Wikipedia <http://en.wikipedia.org/wiki/Regression_analysis>`_.


    Parameters
    ----------
    x:  ndarray
        the population data on which the fit is performed (indepent variable)

    y:  ndarray
        the measurments

    A:  float
        intercept

    B:  float
        slope

    Return
    --------
    sd_intercept:   float
                    standard deviation of the intercept
    sd_slope:   float
                standard deviation of the slope

    """
    N = len(x)
    sigma_y = np.sqrt(((y-A-B*x)**2).sum()/(N-2))
    delta = N*(x**2).sum()-(x.sum())**2
    sd_intercept = sigma_y*np.sqrt((x**2).sum()/delta)
    sd_slope = sigma_y*np.sqrt(N/delta)

    return sd_intercept, sd_slope
