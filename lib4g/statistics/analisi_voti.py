# -*- coding: utf-8 -*-
"""
Routines per l'analisi statistica dei voti
"""
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import fsolve

def analisys(data,
             correggi = True,
             max_voto = 33.0,
             media_des = 25.0,
             no_voto_str='NA'):
    """
    Routine per l'analisi dei voti.
    Presenta una serie di dati statistici sui voti e, se rischiesto, corregge
    i voti in modo che la media dei voti, calcolata sui soli voti sufficienti,
    sia pari a un dato obbiettivo.

    Parameters
    ------------
    data: np.array
        array dei voti; possono essere incluse stringhe che identificano
        voti mancanti

    correggi: bool, optional
        se True, calcola una correzzione alla distribuzione dei voti che sposti
        la media ad un valore prefissato

    max_voto:float
        il voto massimo possibile, corrispondente a trenta e lode. Viene
        usato per dividere in gruppi i voti. Default: 33.0

    media_des:float, optional
        la media desiderata se correggi==True. Default: 25.0

    no_voto_str:string
        stringa che identifica i voti mancanti, viene usata per filtrare i
        dati e operare solo sui dati numerici. Default: 'NA'

    Returns
    --------
    """
    output = np.empty_like(data)
    #conversione dei dati in formato float ed eliminazione voti non validi
    data_masked = data[data != no_voto_str]
    voti = data_masked.astype(float)
    ##########################################################################
    #analisi
    ##########################################################################
    voti_suff = voti[voti>=18.0]
    nvoti = float(len(voti))
    nvoti_suff = float(len(voti_suff))
    pct_voti_suff=nvoti_suff/nvoti
    media_voti = voti.mean()
    media_voti_suff = voti_suff.mean()
    print('DATI STATISTICI ORIGINALI')
    print("Numero totale di voti validi:", nvoti)
    print("Media voti:", media_voti)
    print("Media voti sufficienti:", media_voti_suff)
    print('Numero di voti suffcienti:', nvoti_suff)
    print("Percentuale voti sufficienti:", pct_voti_suff*100,'%')
    print(80*'-')
    ##########################################################################
    #correzione
    ##########################################################################

    if correggi:
        print('DATI DOPO CORREZIONE')
        #shift media
        shift = round(fsolve(correct,3.0,args=[voti, media_des, max_voto]))
        voti_nuovi = voti + shift
        voti_nuovi = tronca_voti(voti, voti_nuovi, max_voto)
        nvoti_suff_nuovi = len(voti_nuovi[voti_nuovi>=18])
        pct_voti_suff_nuovi = nvoti_suff_nuovi/nvoti
        media_voti_nuovi = voti_nuovi.mean()
        media_voti_suff_nuovi = voti_nuovi[voti_nuovi>=18].mean()
        print('Fattore di spostamento:',shift)
        print ('Nuova media voti:',media_voti_nuovi)
        print ('Nuova media voti sufficienti:',media_voti_suff_nuovi)
        print ('Nuovi numeri di voti sufficienti:',nvoti_suff_nuovi )
        print ('Nuova percentuale voti sufficienti:', pct_voti_suff_nuovi*100,
               '%')
        #modifica del vettore con le stringhe
        for i in range(len(data)):
            if data[i]==no_voto_str:
                output[i] = no_voto_str
            else:
                voto_old = float(data[i])
                voto_new = voto_old + shift
                if voto_old == 0:
                    voto_new = 0.0
                if voto_new >30 and voto_old<=30:
                    voto_new = 30.0
                if voto_new > max_voto:
                    voto_new = max_voto
                output[i] = str(voto_new)

    #grafico
    bins = np.arange(max_voto+1)
    fig = plt.figure(figsize=(12,12))
    plt.subplot(211)
    plt.hist(voti,bins=bins, align='left')
    plt.title('Voti prima della correzzione')
    plt.ylabel('Conteggio')
    ylims = plt.ylim()
    plt.vlines(media_voti, ylims[0],ylims[1],colors = u'r',linewidth=2)
    plt.xticks(bins)
    plt.subplot(212)
    plt.title('Voti dopo la correzzione')
    plt.ylabel('Conteggio')
    plt.xlabel('Voti')
    plt.hist(voti_nuovi,bins=bins, align='left')
    plt.xticks(bins)
    ylims = plt.ylim()
    plt.vlines(media_voti_nuovi, ylims[0],ylims[1],colors = u'r',linewidth=2)
    plt.savefig('distribuzione_voti.pdf',bbox_inches='tight')
    np.savetxt('voti_nuovi.txt', output, fmt="%s")
    return output



def correct(x,args):
    """
    Fuzione da azzerare per calcolare la correzione della media

    parameters
    -----------
    x:float
        shift della media

    args:list
        args[0]:dati voti
        args[1]:target media
        args[2]:float il massimo voto consentito
    """
    voti = args[0]
    target = args[1]
    max_voto = args[2]
    voti_shift = voti + x
    #elimina i 30 e lode corretti: non vogliamo che non aveva la lode prima
    #la prenda dopo
    voti_shift = tronca_voti(voti, voti_shift, max_voto)
    media_voti_suff = voti_shift[voti_shift>=18.0]
    return media_voti_suff - target

def tronca_voti(voti_vecchi, voti_nuovi, max_voto):
    """
    Confronta i voti vecchi coi voti nuovi e se ci sono voti nuovi che erano
    inferiori a trenta ma che diventerebbero superiori dopo correzione, tronca
    il valore a trenta. Evita che voti inferiori a 30 prendano la lode dopo
    correzzione e lascia i voti paria zero a zero.

    Parameters
    -----------
    voti_vecchi: nd.array
        elenco dei foti vecchi

    voti_new: nd.array
        elenco voti dopo correzione

    max_voto: float
        il massimo voto consentito

    Returns
    --------
    voti_new:nd.array
        i voti corretti

    """
    for i in range(len(voti_vecchi)):
        voto_old = voti_vecchi[i]
        voto_new = voti_nuovi[i]
        if voto_old == 0:
            voto_new = 0.0
        if voto_new > 30 and voto_old <= 30:
            voti_nuovi[i] = 30.0
        if voto_new > max_voto:
            voti_nuovi[i] = max_voto

    return voti_nuovi
