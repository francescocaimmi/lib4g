# -*- coding: utf-8 -*-

import numpy as np
from scipy.interpolate import UnivariateSpline
from numpy import nanmean
from numpy import nanstd

def avg_curves(*args, **kwargs):
    """
    A routine to calculate an average curve given a set of curves by
    interpolation of the input curves.
    Works for 1-dimensional curves only.
    Uses
    `SciPy UnivariateSpline
    <https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.
    UnivariateSpline.html>`_
    to perform the interpolation duties.

    Parameters
    -----------

    x:  numpy array, optional
        the list of abscissae at which averaging is to be performed.
        If `None` uses the values from the first curve in *args.
        Defaults to None.
        If some x values lies outside the data range for some curve, a
        warning is issued.

    k:  int, optional
        Degree of the smoothing spline. Must be <= 5. Defualts to 1.
        It is passed directly to `SciPy UnivariateSpline`

    s:  float or None, optional
        Positive smoothing factor used to choose the number of knots. Number
        of knots will be increased until the smoothing condition is satisfied:
        sum((w[i]*(y[i]-s(x[i])))**2,axis=0) <= s
        If None (default), s=len(w) which should be a good value if 1/w[i]
        is an estimate of the standard deviation of y[i].
        If 0, spline will interpolate through all data points, which is the
        default beahviour (no smoothing)
        It is passed directly to `SciPy UnivariateSpline`

    w:  array_like, optional
        Weights for spline fitting. Must be positive.
        If None (default), weights are all equal.

    args: numpy arrays
        the set of curves, provided as `n`\ :sub:\ `i`x2 numpy arrays where
        `n`\ :sub:\ `i` is the number of data points in the `i`-th curve.

    Returns
    ---------

    avg: numpy array
        an array containing the averaged values of the ordinates of the curve
        Shape: x.shape()
    std: numpy array
        Estimate of the standard deviation estimated for the averaged points
        calculated as an average of the interpolated values obtained at the
        abscissae specified by `x`
        Shape: x.shape()
    """
    #sanitize input

    #define the variables

    k = kwargs.get('k')
    if k == None:
        k = 1

    s = kwargs.get('s')
    if s == None:
        s = 0
    w = kwargs.get('w')

    #define the abscissae
    x = kwargs.get('x')
    if x is None:
        x = args[0][:,0]


    #build interpolation
    for index, curve in enumerate(args):
        #make sure we have unique x data
        curve_x, mask = np.unique(curve[:,0], return_index = True)
        curve_y = curve[:, 1][mask]

        f = UnivariateSpline(curve_x, curve_y, k = k, s = s, w = w)
        values = f(x)

        #check that all the points lie in the original range otherwise
        #issue a warning
        if np.where(curve[:,0].max()<x[-1])[0].size != 0 or \
           np.where(curve[:,0].min()<x[0])[0].size != 0:
            print "WARNING: extrapolation outside of the defined data range"
            print "attempted for curve", index+1

        if index == 0:
            interpolated_values = values
        else:
            interpolated_values = np.column_stack((interpolated_values,values))

    return nanmean(interpolated_values, axis = 1), \
           nanstd(interpolated_values,axis = 1)
