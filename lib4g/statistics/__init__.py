# -*- coding: utf-8 -*-
"""
The stats package provides several utilities for statistical data analysis.
The routines are suited for task the author often has to perform and are
by no mean complete or organized in a rational way.

"""
import avg_curves
import analisi_voti
import errors
__all__ = ["avg_curves","analisi_voti","errors"]
