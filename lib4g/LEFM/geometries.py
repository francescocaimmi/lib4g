# -*- coding: utf-8 -*-
"""
A module that on a test geomerty basis, provides utilities needed for fracture
mechanics.

Each test geometry is a class providing methods that can be used to calculate
the fracture toughness (sif), the compliance (compliance) and the energy
calibration factor when available.
"""
import os
import numpy as np
from scipy.integrate import quad
from scipy.interpolate import interpn

class geometry(object):
    """
    A generic geometry template
    
    Parameters
    -----------
    
    None
    
    """

    def __init__(self):
        pass

    def compliance (a):
        """
        Method to calculate the compliance :math:`C` for a given crack length, 
        based on Irwin-Kies equation and the G-K relationship.
        
        .. math::
            
            G = \\frac{1}{2 B} P^2 \\frac{\\partial C}{\\partial a} =
            \\frac{K^2}{E^*}
            
        Therefore it will work only for pure mode specimens. 
        In particular for the method to work, a `SIF` method must be defined in 
        the class, giving the unique stress intensity factors for given crack
        lenght and load.
        
        Parameters
        ----------
        a: array-like
            An array of crack lengths where the compliance is to be evaluated.
            Can be a float.
        
        Returns
        C: array-like or float
            The compliance. If a is a number, is a single number, otherwise
            is 
        
        """
        pass

    def Ysquare(self,a):
        """
        Gives the square of the shape factors for given crack length. For
        internal use only, users should stick to Y. It is needed by methods
        that use integration routine to find compliance or energy calibration
        factors

        Parameters
        -----------
        aw: ndarray
            crack lenghts

        Returns
        --------
        Y**2:  ndarray
            squared shape factors

        """

        return self.Y(a)**2

    def phi(self,a):
        """
        Gives the energy calibration factor for the specimen

        Parameters
        -----------
        a:  ndarray
            crack length

        Returns
        --------
        phi: ndarray
            energy calibration factor

        """

        aw = a/self.w
        try:
            n = len(a)
            C = np.zeros(n)
            for i in xrange(n):
                C[i] =  quad(self.Ysquare,0,aw[i])[0]+self._c0
        except TypeError:
            C = quad(self.Ysquare,0,aw)[0]

        Y2 = self.Ysquare(a)
        print Y2
        phi = C/Y2*aw

        return phi

class A4B(geometry):
    """
    A class representing the asymmetric four point bending specimen for
    mixed mode testing.
    Results are based on MY He and JW Hutchinson work :cite:`he2000asymmetric`.
    They hold within some percent of accuracy for b1/W>1.4

    Parameters
    -----------
    B: float
        specimen thickness

    w:  float
        specimen width
    S:  float
        specimen total span (S=b1+b2)
    b1: float
        shortest distance between the load line and a load point
    c: float
        distance between the load and crack lines
    E:  float, optional
        specimen elastic modulus. Defaults to 1.

    Additional Attributes
    ----------------------
    b2:float
        S-b1
    """
    def __init__(self,B,w,S,b1,c,E=1.0):
        self.B = float(B)
        self.w = float(w)
        self.S = float(S)
        self.b1 = float(b1)
        self.E = float(E)
        self.c = float(c)
        self.b2 = S-b1
        self._c0 = 0

    def ModeI_SIF(self,a,F):
        """
        Calculates the mode I stress intensity factor (SIF)

        Parameters
        -----------
        a:  ndarray
            crack lengths (possibily an array of crack lenghts)
        F:  ndarray
            force at which the SIF is to be evauated. Possibily an array
            of the same length as `a`

        Returns
        --------
        SIF: ndarray
            the mode I stress intensity factor for the given geometry and loads
            If `a` and  `F` are arrays of the same length, is a an array of
            different SIF for different values of `a` (corresponding to F). If
            both `a` and `F` are scalars, it is a scalar.

        Rises an error if `a` and `F` are both arrays not of the same shape.
        """
        Q = (F/self.B)*(self.b2-self.b1)/(self.S)
        sigma = Q/self.w
        Y = self.ModeI_Y(a)

        return sigma*np.sqrt(a*np.pi)*Y

    def ModeII_SIF(self,a,F):
        """
        Calculates the mode II stress intensity factor (SIF)

        Parameters
        -----------
        a:  ndarray
            crack lengths (possibily an array of crack lenghts)
        F:  ndarray
            force at which the SIF is to be evauated. Possibily an array
            of the same length as `a`

        Returns
        --------
        SIF: ndarray
            the mode I stress intensity factor for the given geometry and loads
            If `a` and  `F` are arrays of the same length, is a an array of
            different SIF for different values of `a` (corresponding to F). If
            both `a` and `F` are scalars, it is a scalar.

        Rises an error if `a` and `F` are both arrays not of the same shape.
        """
        Q = (F/self.B)*(self.b2-self.b1)/(self.S)
        sigma = Q/self.w
        Y = self.ModeII_Y(a)

        return sigma*np.sqrt(a*np.pi)*Y

    def ModeI_Y(self,a):
        """
        Gives the ModeI shape factors for given crack length over thickness
        ratio

        Parameters
        -----------
        aw: ndarray
            crack lenghts

        Returns
        --------
        Y:  ndarray
            shape factors

        """
        aw = a/self.w
        F1 = 1.122-1.121*aw+3.740*aw**2+3.873*aw**3-19.05*aw**4+22.55*aw**5

        return 6*self.c*F1/self.w

    def ModeII_Y(self,a):
        """
        Gives the Mode II shape factors for given crack length over thickness
        ratio

        Parameters
        -----------
        aw: ndarray
            crack lenghts

        Returns
        --------
        Y:  ndarray
            shape factors

        """
        aw = a/self.w
        F2 = 7.264-9.37*aw+2.74*aw**2+1.87*aw**3-1.04*aw**4
        k = self.w**(0.5)*aw**(1.5)/(np.sqrt(a*np.pi)*(1-aw)**(0.5))
        return k*F2

class DCB_isotropic(object):
    """
    A class representing a DCB made of a homogenous isotropic material.
    It is based on the solution by Kanninen :cite:`Kanninen1973`.
    The reader is referred to that work for the exact definition of the symbols.
    The results are unrealiable for small valus of crack length, when the 
    B in DCB is no longer a beam.
    
    A class for DCB made wth composites materials and following composite 
    material standards will be probably implemented later.
    
    Parameters
    -----------
    
    h: float
        half the specimen thickness (thickness of one arm).
    
    L: float
        the specimen length as measured from the load point to the end.
    
    b: float
        the out of plane thickness.
    
    E: float, optional
        the elastic modulus. Defaults to 1.
        
    Attributes
    -----------
    
    k: float
        the elastic modulus of the model Wrinkler foundation, calculated as
        k = 2 E b/h.
        
    lamb: float
        the eigenvalue of the beam equation.
    """
    
    def __init__(self, h, L, b, E=1):
        self.h = h
        self.L  = L
        self.b = b
        self.E = E
        self.k = 2 * E * b / h
        self.lamb = 6**(1.0/4.0)/h
        
    def compliance(self, a):    
        """
        Returns the compliance of the specimen

        Parameters
        -----------
        a:  array-like
            values at which the compliance should be sampled

        Returns
        --------
        comp: nd.array or float
            the compiance values correspong to the crack lenght values in a.
            Is a single value if a is a single value.
        """
        try:
            na = len(a)
        except TypeError:
            na = 1
            a = np.array([a])
        comp = np.zeros_like(a)
        
        for i in range(na):
            c = self.L - a[i]
            l = self.lamb
            sh = np.sinh(l*c)
            ch = np.cosh(l*c)
            s = np.sin(l*c)
            c = np.cos(l*c)
            den = sh**2 - s**2
            PHI = (2.0/(l**3 * self.h**3 )) * \
                      (2.0* l**3 * a[i]**3 + \
                      6.0 * l**2 * a[i]**2 * ((sh * ch + s * c)/den) +\
                      6.0 * l *a[i] *((sh**2 + s**2)/den) + \
                      3.0* ((sh * ch - s *c)/den))
            comp[i] = PHI / (self.E * self.b)
        
        if len(comp) != 1:
            return comp
        else:
            return comp[0]

    def SIF(self, a, P):
        """
        Calculates the stress intensity factor (SIF).

        Parameters
        -----------
        
        a:  ndarray
            Crack length, a single value or an array.
            
        F:  ndarray
            force at which the SIF is to be evauated. Possibily an array
            of the same length as `a`.

        Returns
        --------
        
        SIF: ndarray or scalar
            the stress intensity factor for the given geometry and loads
            If `a` and  `F` are arrays of the same length, is a an array of
            different SIF for different values of `a` (corresponding to F). If
            both `a` and `F` are scalars, it is a scalar.
        """
        if hasattr(P,"__len__") and hasattr(a,"__len__"):
            assert len(P)==len(a), "if both a and P are vectors, they must"+\
                                    "have the same shape"
        c = self.L - a
        l = self.lamb
        sh = np.sinh(l*c)
        ch = np.cosh(l*c)
        s = np.sin(l*c)
        c = np.cos(l*c)
        den = sh**2 - s**2
        
        SIF = 2*np.sqrt(3) *(P/(l * self.b * self.h**(3.0/2.0))) * \
                (l * a * ((sh**2+s**2)/den)+  ((sh * ch - s *c)/den))
        
        return SIF
        

class MMB(geometry):
    """
    A class representing the Mixed Mode Bending geometry,i.e. SEN(B) with crack
    offset w.r.t. the load line.

    The formulas are taken from handbook by Theo Fett :cite:`Fett2008`.
    Misalignment of the load with respect to the mid plane is not
    considered; the data are valid in the ranges -0.3<e/w<0.3 and 0.1<a/w>0.8.
    The data are exact for L/W=2, 2.5 and 5 and interpolated linearly in
    this range (L/W=2 is the case of ISO 13586).

    Parameters
    -----------
    b:  float
        specimen thickness
    w:  float
        half specimen width
    L:  float
        half specimen length
    e:  float
        distance between the crack plane and the midplane
    E:  float, optional
        specimen elastic modulus. Defaults to 1.

    Attributes
    -----------
    _c0:    float
        uncracked compliance of half specimen per unit modulus
        
    C0: float
        specimen compliance C0 = E*_c0

    """
    def __init__(self,b,w,L,e, E=1):
        self.b = b
        self.w = w
        self.L = L
        self.e = e
        self.E = E
        self._c0 = L/(b*w)
        #data to interpolate the SIFs
        self._ews = np.array([-0.3,-0.1,-0.04,-0.025,0.0,0.025,0.04,0.1,0.3])
        self._lws=np.array([2.0,2.5,5.0])
        self._aws= np.arange(0.1,0.9,0.1)
        #load the tables with the shape factors data
        self._fIprime = np.load(
            os.path.join(os.path.dirname(__file__),'mmb_FIprime.npy'))
        self._fII = np.load(os.path.join(os.path.dirname(__file__),
                                         'mmb_FII.npy'))


    def ModeI_SIF(self,a,F):
        """
        Calculates the mode I stress intensity factor (SIF)

        Parameters
        -----------
        a:  ndarray
            crack lengths (possibily an array of crack lenghts)
        F:  ndarray
            force at which the SIF is to be evauated. Possibily an array
            of the same length as `a`

        Returns
        --------
        SIF: ndarray
            the mode I stress intensity factor for the given geometry and loads
            If `a` and  `F` are arrays of the same length, is a an array of
            different SIF for different values of `a` (corresponding to F). If
            both `a` and `F` are scalars, it is a scalar.

        Rises an error if `a` and `F` are both arrays not of the same shape.
        """
        sigma = 3*F*self.L/(self.w**2*self.b)
        Y = self.ModeI_Y(a)

        return sigma*np.sqrt(a*np.pi)*Y

    def ModeII_SIF(self,a,F):
        """
        Calculates the mode II stress intensity factor (SIF)

        Parameters
        -----------
        a:  ndarray
            crack lengths (possibily an array of crack lenghts)
        F:  ndarray
            force at which the SIF is to be evauated. Possibily an array
            of the same length as `a`

        Returns
        --------
        SIF: ndarray
            the mode I stress intensity factor for the given geometry and loads
            If `a` and  `F` are arrays of the same length, is a an array of
            different SIF for different values of `a` (corresponding to F). If
            both `a` and `F` are scalars, it is a scalar.

        Rises an error if `a` and `F` are both arrays not of the same shape.

        """
        sigma = 3*F*self.L/(self.w**2*self.b)
        Y = self.ModeII_Y(a)

        return sigma*np.sqrt(a*np.pi)*Y

    def ModeI_Y(self,a):
        """
        Gives the ModeI shape factors for given crack length over thickness
        ratio

        Parameters
        -----------
        aw: ndarray
            crack lenghts

        Returns
        --------
        Y:  ndarray
            shape factors

        """
        ew = self.e/self.w
        aw = a/self.w
        lw = self.L/self.w
        print [ew,aw,lw]
        FIprime = interpn((self._ews, self._aws,self._lws), self._fIprime,
                            [ew,aw,lw])

        return FIprime/(1-aw)**(3.0/2.0)

    def ModeII_Y(self,a):
        """
        Gives the ModeII shape factors for given crack length

        Parameters
        -----------
        a: ndarray
            crack lenght

        Returns
        --------
        Y:  ndarray
            shape factors

        """
        ew = self.e/self.w
        aw = a/self.w
        lw = self.L/self.w
        FII = interpn((self._ews, self._aws,self._lws), self._fII,
                            [ew,aw,lw])

        return FII




class DENT(geometry):
    """
    A class representing the Double Edge Notch Tensile (DENT) test specimens

    The formulas are taken from the Rooke-Cartwright handbook, and are valid
    for all the crack lenghts for a lenght over width ratio larger than 3

    Parameters
    -----------
    b:  float
        specimen thickness
    w:  float
        half specimen width
    L:  float
        specimen length
    E:  float, optional
        specimen elastic modulus. Defaults to 1.

    Attributes
    -----------
    _c0:    float
        uncracked compliance of half specimen per unit modulus
    C0: float
        specimen compliance C0 = E*_c0

    """
    def __init__(self,b,w,L, E=1):
        self.b = b
        self.w = w
        self.L = L
        self.E = E
        self._c0 = L/(b*w)

    def SIF(self,a,F):
        """
        Calculates the stress intensity factor (SIF)

        Parameters
        -----------
        a:  ndarray
            crack lengths (possibily an array of crack lenghts)
        F:  ndarray
            force at which the SIF is to be evauated. Possibily an array
            of the same length as `a`

        Returns
        --------
        SIF: ndarray
            the stress intensity factor for the given geometry and loads
            If `a` and  `F` are arrays of the same length, is a an array of
            different SIF for different values of `a` (corresponding to F). If
            both `a` and `F` are scalars, it is a scalar.

        Rises an error if `a` and `F` are both arrays not of the same shape.
        """
        sigma = F/(self.b*2*self.w)

        Y = self.Y(a)

        return sigma*np.sqrt(np.pi*a)*Y


    def Y(self,a):
        """
        Gives the shape factors for given crack length over thickness ratio

        Parameters
        -----------
        aw: ndarray
            crack lenghts

        Returns
        --------
        Y:  ndarray
            shape factors

        """
        aw = a/self.w

        return (1.12-0.56*aw-0.015*aw**2 +0.091*aw**3)/np.sqrt(1-aw)

    def _integrand(self,a):
        """
        Returns the integrand to be used in the calculation of the energy
        calibration factor

        Parameters
        -----------
        a: ndarray
            crack length

        Returns
        --------
        integrand: ndarray
            the integrand
        """

        a_w=a/self.w
        return a_w*self.Y(a)**2

    def phi(self,a):
        """
        Gives the energy calibration factor for the specimen

        Parameters
        -----------
        a:  ndarray
            crack length

        Returns
        --------
        phi: ndarray
            energy calibration factor

        """

        aw = a/self.w
        try:
            n = len(a)
            C = np.zeros(n)
            for i in xrange(n):

                C[i] =  np.pi*quad(self._integrand,0,aw[i])[0]+self.b*self._c0

        except TypeError:
            C = np.pi*quad(self._integrand,0,aw)[0]+self.b*self._c0

        Y2 = self.Ysquare(a)
        phi = C/(np.pi*aw*Y2)

        return phi

class SENB(geometry):
    """
    A class representing SENB samples, as described by ISO 13586.

    Parameters
    -----------
    w:  float
        the specimen width (ligament direction)
    h:  float
        the specimens thickness, out of plane
    L:  float,optional
        the specimen span. Optional as unneded for K_IC calculations.
        Defualts to 1
    E:  float, optional
        the elastic modulus. Defaults to 1
    v:  float,optional
        Poisson's coefficient, defaults to 0.3

    Attributes
    ------------
    _c0:float
        uncracked specimen compliance per unit modulus (C0=E*_c0)

    C0: float
        uncraked complaince calculated according to simple beam theory

    """

    def __init__(self, w, h, L = 1, E = 1, v = 0.3):
        self.w = float(w)
        self.h = float(h)
        self.L = float(L)
        self.E = float(E)
        self.v = float(v)
        self._c0  = (1.0/4.0)*self.L**3/(self.h*self.w**3)
        self.C0 = self._c0/E
        
    def compliance(self,a):
        """
        Returns the compliance of the specimen
        
        **not tested!!!!**

        Parameters
        -----------
        a:  nd_array
            values at which the compliance should be sampled

        Returns
        --------
        out: nd_array
            the compiance values correspong to the crack lenght values in a
            
        """
        try:
            n = len(a)
        except:
            a = np.array([a])
            n = 1
            
        out = np.zeros(a.shape)
        Eprime = self.E/(1-self.v**2)
        k = 2.0/(Eprime*self.h)

        for i in range(n):

            out[i] = self.C0+k*quad(self.Ysquare,0,a[i])[0]

        if n == 1:
            return out[0]
        else:
            return out

    def SIF(self,a,F):
        """
        Calculates the stress intensity factor (SIF)

        Parameters
        -----------
        a:  ndarray
            crack lengths (possibily an array of crack lenghts)
        F:  ndarray
            force at which the SIF is to be evauated. Possibily an array
            of the same length as `a`

        Returns
        --------
        SIF: ndarray
            the stress intensity factor for the given geometry and loads
            If `a` and  `F` are arrays of the same length, is a an array of
            different SIF for different values of `a` (corresponding to F). If
            both `a` and `F` are scalars, it is a scalar.

        """
        #TODO Rises an error if `a` and `F` are both arrays not of the same shape.
        sigma = F/(self.h*np.sqrt(self.w))

        Y = self.Y(a)

        return sigma*Y

    def Y(self,a):
        """
        Gives the shape factors for given crack length over thickness ratio

        Parameters
        -----------
        aw: ndarray
            crack lenghts

        Returns
        --------
        Y:  ndarray
            shape factors

        """
        aw = a/self.w

        return 6*np.sqrt(aw)*(1.99-aw*(1-aw)*(2.15-3.93*aw+2.7*aw**2))/\
                ((1+2*aw)*(1-aw)**(3.0/2.0))

    def phi(self, a):
        """
        Gives the energy calibration factor for the specimen.

        Parameters
        -----------
        a:  ndarray
            crack length

        Returns
        --------
        phi: ndarray
            energy calibration factor

        """

        aw = a/self.w
        try:
            n = len(a)
            C = np.zeros(n)
            for i in xrange(n):
                C[i] =  2*quad(self.Ysquare,0,aw[i])[0]+self.h*self._c0
        except TypeError:
            C = 2*quad(self.Ysquare,0,aw)[0]+self.h*self._c0

        Y2 = self.Ysquare(a)
        phi = C/(2*Y2)

        return phi

    def phi13586(self, a):
        """
        Calculates the energy calibration factors following the
        formula given in ISO 13586, valid only for L/W=4.

        Parameters
        -----------
        a: ndarray
            a list of crack lengths

        Returns
        --------
        phi: ndarray
            energy calibration factor

        """

        aw = a/self.w
        A = (16*aw**2/(1-aw)**2)*(8.9 - 33.717*aw + 79.616*aw**2 - \
            112.952*aw**3+84.815*aw**4-25.672*aw**5)
        dA = (16*aw**2/(1-aw)**2)*(-33.717 + 159.232*aw-338.856*aw**2+\
            339.26*aw**3-128.36*aw**4)+ 16*((2*aw*(1-aw)+2*aw**2)/(1-aw)**3)*\
            (8.9 - 33.717*aw + 79.616*aw**2 - \
            112.952*aw**3+84.815*aw**4-25.672*aw**5)

        phi = (A +18.64)/dA

        return phi
