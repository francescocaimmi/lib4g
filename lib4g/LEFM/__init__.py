# -*- coding: utf-8 -*-
"""
A set of routines to work with linear elastic fracture mechanics
"""
import geometries
import ISO17281
__all__=['geometries', 'ISO17281']