# -*- coding: utf-8 -*-
"""
Routines to work with the ISO 17281 standard: Plastics — Determination of
fracture toughness (GIC and KIC) at moderately high loading rates (1 m/s)
"""
import numpy as np
from scipy.optimize import minimize

def model(t,x0,x1,x2,x3):
    """
    The model for  fitting the load-time traces:
    c1(t-t0)+c2(t-t0)**n

    Parameters
    ------------

    t: numpy array
        time

    xi: floats
        parameters array, given in the following order[c1,c2,t0,n]

    Return
    -------
    value: float
        the model value at given time
    """
    return x0*(t-x2)-x1*(t-x2)**x3

def force_obj(time,force):
    """
    Objective function for the force fit

    Parameters
    ------------
    time: ndarray
        experimental times
    force: ndarray
        experimental force

    Returns
    --------
    obj: function
        callable object depending on the fit parameters giving the
        residual sum of squares
    """
    def obj(x):
        return (np.linalg.norm(force-model(time,x[0],x[1],x[2],x[3])))**2

    return obj

def forcefit(time,force,bounds = [(None, None), (0,None),(None,None), (0,10)],
            x0 = [1000,100,0.1,5]):

    """
    Fits the force-time traces to the equation

    c1(t-t0)+c2(t-t0)**n

    where c1,c2,t0 and n are the fit parameters

    Parameters
    ----------
    time: numpy array
        times array
    force: numpy array
        forces
    bounds: sequence, optional
        Bounds for variables. (min, max) pairs for each parameter in the order
        [c1,c2,t0,n],
        defining the bounds on that parameter.
        Use None for one of min or max when there is no bound in that
        direction.
    x0: numpy array, optional
        initial guess for the parameters


    Returns
    -------
    x: numpy array
        the value of the fit parameters in the following order [c1,c2,t0,n]

    rss: float
        the residual sum of squares

    residue:ndarray
        the residue of the fit
    """
    obj = force_obj(time,force)
    x = minimize(obj,x0,method = 'SLSQP', bounds = bounds)
    if x['success'] == False:
        print "Minimization failed"
    rss = model(time,*x['x'])

    return x['x'],x['fun'],rss
