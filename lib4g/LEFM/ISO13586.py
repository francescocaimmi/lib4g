# -*- coding: utf-8 -*-
"""
Routines to work with the ISO 13586 standard: Plastics — Determination of
fracture toughness (GIC and KIC) - Linear elastic approach
"""
#third party imports
import numpy as np

from scipy import stats
from scipy.integrate import cumtrapz
from scipy.interpolate import UnivariateSpline
from scipy.optimize import brentq
from scipy.optimize import fmin

#lib4g imports

from lib4g.misc.functions import subtract_functions


def line(m,q):
    """
    A function to calulate lines in the form m*x+q

    c1: float
        stiffness
    t0: float
        zero time

    Returns
    --------
    aline: function
        a callable object that can be used to calculate the line
    """
    def aline(x):
        """
        Parameters
        -----------
        t: ndarray
            times at which it must be calculated
        """

        return m*x+q
    return aline

def linear_fit(disp,force, ymin = 1.0/3.0, ymax = 2.0/3.0):
    """
    Perform a linear fit to the force data.

    Parameters
    -----------
    disp:   numpy array
        displacement of the crosshead

    force:  numpy array
        force data

    ymin: float, optional
        the fraction of np.max(force) to be considered as the first bound
        of the fitting interval. Defaults to 1/3

    ymax: float, optional
        the fraction of np.max(force) to be considered as the second bound
        of the fitting interval. Defaults to 2/3

    Returns
    --------
    slope:float
        slope of the fit

    intercept:float
        fit intercept

    """
    max_index = np.argmax(force)
    Fmax = force[max_index]
    #mask
    m = np.logical_and(force[:max_index+1]>Fmax*ymin,
                       force[:max_index+1]<Fmax*ymax)

    f = force[m]
    d = disp[m]
    #check for NaN and issue a warning, but otherwise remove NaN
    if any(np.isnan(disp)) or any(np.isnan(force)):
        print "Warning: NaNs value in force-displacement arrays"
        m = np.logical_and(~np.isnan(d),~np.isnan(f))
    else:
        m = np.array([True for i in xrange(len(f))])

    slope, intercept, r_value, p_value, std_err = stats.linregress(d,f)

    return slope, intercept

def energy(disp, force):
    """
    Integrates the load displacement trace and returns the corresponding energy
    curve using scipy cumtrapz routine

    Parameters
    -----------
    disp:numpy array
        displacement of the crosshead

    force:numpy array
        force data

    Returns
    --------
    U:ndarray
        the corresponding energy
    """

    return cumtrapz(force, disp, initial = 0)

def critical_load(d, F, ymin = 1.0/3.0, ymax = 2.0/3.0, spline = False,
                  full_output = False):
    """
    Finds the critical load for fracture tests and performs the check for
    linearity

    Parameters
    -----------

    d:numpy array
        displacement of the crosshead

    F:numpy array
        force data

    ymin: float, optional
        the fraction of np.max(force) to be considered as the first bound
        of the fitting interval. Defaults to 1/3.

    ymax: float, optional
        the fraction of np.max(force) to be considered as the second bound
        of the fitting interval. Defaults to 2/3.

    spline: bool, optional
        wheter or not to use an interpolated representation of the data or
        the actual data to calculate the critical load. Defualts to False, i.e.
        to use the actual data. Can be used to get estimates for small datasets

    full_output: bool, optional
        wheter to return full output or not. Defualts to False

    Returns
    --------
    Fq: float
        critical load

    lcheck: bool
        if true the behaviour is linear, otherwise linearity criteria are
        not met

    If full out is specified it also returns

    Fmax: float
        maximum load

    F5: float
        load at 5% intersection

    S: the slope of the linear fit

    S5: the slope of the line with a compliance increased of 5%

    F0: the intercept of the linear fit

    """
    #find maximum load and the corresponding index
    imax = np.argmax(F)#i stands for index
    Fmax = F[imax]
    dmax = d[imax]

    #find the coefficient of the linear fit, slope and intercept
    m, q = linear_fit(d, F, ymin = ymin, ymax = ymax)
    line95 = line(m/1.05,q)
    #define a spline representation of the data
    mask = F > Fmax*ymin
    myspline = UnivariateSpline(d[mask], F[mask], k = 1, s = 0)
    #define a function to be zeroed to find the model / line at 95 %
    #compliance intersection
    tozero = subtract_functions(myspline,line95)
    #calculate the intersection
    try:
        #brentq works better for curves with a long descending region
        #but requires that the values of tozero ad the boundaries be
        #of different sign, which may not be the case for traces with sudden
        #load droaps
        d5 = brentq(tozero, d[mask][0],d[mask][-1])
    except ValueError as e:
        #if brentq fails we resort to fmin
        print "Brentq finding roots algorithm failed with"
        print e.message
        print "Using fmin. Please check if the results are sensible"
        d5 = fmin(tozero,Fmax)

    i5 = np.argsort(abs(d-d5))[0]
    #find the load at d5
    if spline:
        F5 = myspline(d5)
    else:
        F5 = F[i5]
    #determine initiation
    #the second check is useful to avoid false positives in the case the
    #intersection is in the low loads region
    if d5<dmax :#and d5>0.7*dmax:
        dq = d[i5]
        Fq = F5
        iq = i5
    else:
        dq =dmax
        Fq = Fmax
        iq = imax #index of critical load
    #check linearity criterion
    if F5/Fmax < 1.1:
        lcheck = True
    else:
        lcheck = False

    if full_output:

        return Fq, lcheck, Fmax, dmax, F5, d5, m, m/1.05, q

    else:

        return Fq, lcheck


