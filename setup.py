#!/usr/bin/env python
# -*- coding: utf-8 -*-

#`python setup.py develop` can be used to make only links to the source
#directories so that changing in the files are immediately applied.

#from distutils.core import setup
from setuptools import setup

#from distutils.extension import Extension
#from Cython.Build import cythonize
#if for some reason import
v='1.0'

setup(name = "Lib4g",
      version = v,
      description ="Francesco Caimmi's routines for data analysis",
      author = 'Francesco Caimmi',
      author_email = 'francesco.caimmi@polimi.it',
      packages = ['lib4g',
                      'lib4g.bin',  #questo è sbagliato ma lo sistemeremo poi
                      'lib4g.composites',
                      'lib4g.LEFM',
                      'lib4g.misc',
                      'lib4g.statistics',
                  ],
      install_requires = ['numpy>=1.5',
                          'scipy',
                          'sphinxcontrib-bibtex',
                          'matplotlib'],
#devo sistemare la faccenda degli script un giorno lo farò
#      scripts=['bin/blalalal'],
)
